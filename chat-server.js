
// Author: Carlos Gonzalez
// Class: CSE 330: Rapid Prototype and Development
// Date: 4/27/2016
// Creative Project


// Require the packages we will use:
var http = require("http"),
	socketio = require("socket.io"),
	fs = require("fs");

//Our users
var users = {};
//The names of the rooms.
var roomNames = ["general"];
//Our main JSON object containing all of the information for this server.
var allRooms = {
				"allRooms": [{
					"owner" : "",
					"name" : "general",
					"people" : [],
					"banned" :[],
					"pass" : ""
				}]
			};
 
// Listen for HTTP connections.  This is essentially a miniature static file server that only serves our one file, client.html:
var app = http.createServer(function(req, resp){
	// This callback runs when a new connection is made to our HTTP server.
	fs.readFile("client.html", function(err, data){
		// This callback runs when the client.html file has been read from the filesystem.
 
		if(err) return resp.writeHead(500);
		resp.writeHead(200);
		resp.end(data);
	});
});
app.listen(3456);
 
// Do the Socket.IO magic:
var io = socketio.listen(app);
io.sockets.on("connection", function(socket){
	// This callback runs when a new Socket.IO connection is established.
 	socket.on("join",function(username){
 		//Joining the general room
 		users[socket.id] = username;
 		socket.join(username);
 		socket.join("general");
 		allRooms.allRooms[0].people.push({name:users[socket.id], socket: socket.id});

 		//Replying a succesfull join.
 		socket.emit("update","You have successfully connected to the General chat room.");
 		io.sockets.emit("updateUsers",users,allRooms);
 		io.sockets.emit("updateRooms",roomNames,allRooms);
 	});

 	//Function used to send messages.
 	socket.on("send",function(data){
 		io.to(data["namespace"]).emit("chat",users[socket.id],data["message"] );
 	});

 	//Function used to kick users.
 	socket.on("kick",function(data){
 		io.sockets.emit("updateRooms",roomNames,allRooms);
 		io.sockets.emit("updateUsers",users,allRooms);
 	 	io.sockets.in(data["name"]).emit("kicked",data["room"]);
  	});

 	//Function used to ban users.
  	socket.on("ban",function(data){
  		for(var i = 0; i < allRooms.allRooms.length; i++)
		{
			if(allRooms.allRooms[i].name == data["room"]){
				var person = {name: data["name"]};
				allRooms.allRooms[i].banned.push(person);
			}
		}

		//Replying successfully to the client.
 		io.sockets.emit("updateRooms",roomNames,allRooms);
 		io.sockets.emit("updateUsers",users,allRooms);
  	});

  	 	//Function in charge of switching for a given user.
 	socket.on("switchRoom",function(data){

 		//Leaving and joining the newspace.
 		socket.leave(data["oldRoom"]);
 		socket.join(data["newRoom"]);


 		//Removing the user from their previous room and joining current room.
 		for(var i = 0; i < allRooms.allRooms.length; i++)
		{
			//Check if the user is banned.
			if(allRooms.allRooms[i].name == data["newRoom"]){
				for(var j = 0; j< allRooms.allRooms[i].banned.length;j++){
					console.log(allRooms.allRooms[i].banned[j]["name"]);
					if(allRooms.allRooms[i].banned[j]["name"] == users[socket.id]){
						socket.emit("wrongpass","You are banned from this room!");
						return;
					}
				}
			}
			
			//Removing from the old room.
			for(var j = 0; j<allRooms.allRooms[i].people.length; j++){
				if(allRooms.allRooms[i].people[j]["name"] == users[socket.id]){
 					allRooms.allRooms[i].people.splice(j,1);
 				}
			}  
		}
		
		//Adding the user to the new room.
		for(var i = 0; i < allRooms.allRooms.length; i++)
		{
			if(allRooms.allRooms[i].name == data["newRoom"])
			{
				var person = {name: users[socket.id], socket: socket.id};
				 allRooms.allRooms[i].people.push(person);				
			}
		}
		 

		//Replying successfully to the client.
		io.to(data["newRoom"]).emit("joinRoom",data["newRoom"]);
 		socket.broadcast.to("general").emit("update",users[socket.id] + " has left the room.");
 		io.sockets.emit("updateRooms",roomNames,allRooms);
 		io.sockets.emit("updateUsers",users,allRooms);
 	});

 	//Function in charge of creating a new room and adding it to the JSON object.
 	socket.on("createRoom",function(data){
 		
 		//Leaving and joining the newspace.
 		socket.leave(data["oldspace"]);
 		socket.join(data["namespace"]);

 		//Pushing the name of the room.
 		roomNames.push(data["namespace"]);

 		//Removing the user from their previous room.
 		for(var i = 0; i < allRooms.allRooms.length; i++)
		{
			for(var j = 0; j<allRooms.allRooms[i].people.length; j++){
				if(allRooms.allRooms[i].people[j]["name"] == users[socket.id]){
 					allRooms.allRooms[i].people.splice(j,1);
 				}
			}  
		}

		//Adding the new room.
		var newRoom = {
			owner: users[socket.id],
			name: data["namespace"],
			people: [{name: users[socket.id], socket: socket.id}],
			banned: [],
			pass : data["pass"]
		};
		allRooms.allRooms.push(newRoom);

		//Replying successfully to the client.
 		io.to(data["namespace"]).emit("joinRoom",data["namespace"]);
 		socket.broadcast.to("general").emit("update",users[socket.id] + " has left the room.");
 		io.sockets.emit("updateRooms",roomNames,allRooms);
 		io.sockets.emit("updateUsers",users,allRooms);
 	});

 	//Replying to a personal message request.
 	socket.on("personal",function(data){
 		io.sockets.in(data["user"]).emit("personal",users[socket.id],data["message"]);
 		socket.emit("personal",users[socket.id],data["message"]);
 	});

 	//Deleting a room.
 	socket.on("deleteRoom",function(name){
 		for(var i = 0; i < allRooms.allRooms.length; i++)
		{
			if(allRooms.allRooms[i].name == name){
				allRooms.allRooms.splice(i,1);
			}
		}
		io.sockets.emit("updateRooms",roomNames,allRooms);
 	});

 	//Executes upon disconnection of a user.
 	socket.on("disconnect",function(){
 		delete users[socket.id];
		io.sockets.emit("updateRooms",roomNames,allRooms);
 		io.sockets.emit("updateUsers",users,allRooms);
 	});

});
